import React, {useState} from 'react';
import './App.css';
import Email from './components/Email';
import Password from './components/Password';
import Button from './components/Button';


function App() {

  const [address, setEmail] = useState('');
  const [secret, setPassword] = useState('');

        const handleChangePassword = e => {
            setPassword (e.target.value);
          console.log(secret);
        };

        const handleChangeEmail = e => {
            setEmail(e.target.value);
          console.log(address);
        };

        const buttonClicked = ()=> {
          alert (`${address} ${secret}`)
        }
      


  return (
    <div className="App">
      <header className="App-header">
        
        <Email address = {handleChangeEmail}/>
        <Password secret = {handleChangePassword}/>
        <Button done = {buttonClicked} />

      </header>
    </div>
  );
}

export default App;
